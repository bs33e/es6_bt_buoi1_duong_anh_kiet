const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];

let loadColor = (list) => {
  let contentHTML = "";
  list.forEach((color, index) => {
    let contentColor = `
           <button id="${index}" onclick="changeColor(${index})" class="color-button ${color}"></button>
        `;
    contentHTML += contentColor;
  });
  document.getElementById("colorContainer").innerHTML = contentHTML;
};

loadColor(colorList);

document.getElementById(0).classList.add("active");
document.getElementById("house").classList.add(`${colorList[0]}`);

function changeColor(id) {
  var btnColor = document.querySelectorAll("#colorContainer button");
  for (var i = 0; i < btnColor.length; i++) {
    if (id == i) {
      document.getElementById(i).classList.add("active");
      document.getElementById("house").classList.add(`${colorList[i]}`);
    } else {
      document.getElementById(i).classList.remove("active");
      document.getElementById("house").classList.remove(`${colorList[i]}`);
    }
  }
};
